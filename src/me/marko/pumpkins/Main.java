package me.marko.pumpkins;

import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.marko.pumpkins.commands.PumpkinCMD;
import me.marko.pumpkins.utils.Utils;

public class Main extends JavaPlugin implements Listener {

	public void onEnable() {
		Utils.msgConsole("&7Loading Halloween Pumpkins...");
		Utils.msgConsole("&7...");
		Utils.msgConsole("&7Loaded Halloween Pumpkins!");
		
	    this.getServer().getPluginManager().registerEvents(new events(), this);
	    
		this.getCommand("pumpkin").setExecutor(new PumpkinCMD());

	}
	
	public void onDisable() {
		Utils.msgConsole("&cUnloading Halloween Pumpkins...");
		Utils.msgConsole("&c...");
		Utils.msgConsole("&cUnloaded Halloween Pumpkins!");
	}
}
