package me.marko.pumpkins;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;

public class events implements Listener {
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent event) {
		Player p = event.getPlayer();
		Block block = event.getBlock();
		ItemStack item = p.getItemInHand();
		Location location = p.getLocation();
		
		if (item !=null && item.getItemMeta().getDisplayName().equals(ChatColor.translateAlternateColorCodes('&', "&6Pumkinator's Pumpkin"))) {
			block.setType(Material.AIR);
			p.playSound(p.getLocation(), Sound.EXPLODE, 3.0F, 0.5F);
            PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(EnumParticle.EXPLOSION_LARGE, true, (float) location.getX(), (float) location.getY(), (float) location.getZ(), 0, 0, 0, 0, 0, null);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(particles);
            return;

		}
			
		}

}


