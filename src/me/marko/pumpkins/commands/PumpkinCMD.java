package me.marko.pumpkins.commands;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import me.marko.pumpkins.utils.Utils;
import net.md_5.bungee.api.ChatColor;

public class PumpkinCMD implements CommandExecutor {
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		 Player p = (Player)sender;
		 if (p.hasPermission("pumpkin.give")) {
			 if (!(sender instanceof Player)) {
			      System.out.println(ChatColor.RED + "You cannot use this command in the console.");
			      return false;
		    }
			 else
				 if(args.length == 0) {
					 p.sendMessage("");
					 p.sendMessage(Utils.color("&cUsage: /pumpkin give <player> <amount>"));
					 p.sendMessage("");
				 }
			 if(args.length == 1) {
				 p.sendMessage("");
				 p.sendMessage(Utils.color("&cUsage: /pumpkin give <player> <amount>"));
				 p.sendMessage("");
			 }
			 if(args.length == 2) {
				 Player target = Bukkit.getPlayer(args[1]);
				 if (target == null) {
					 p.sendMessage(ChatColor.DARK_RED + "ERROR: " + ChatColor.RED + args[1] + " is offline!");
					 return true;
				 }else
					 p.sendMessage("");
					 p.sendMessage(Utils.color("&cUsage: /pumpkin give <player> <amount>"));
					 p.sendMessage("");
				 
			 }
			 if(args.length == 3) {
				 ItemStack pumpkin = new ItemStack(Material.PUMPKIN);
				 ItemMeta pumpkinMeta = pumpkin.getItemMeta();
				 pumpkinMeta.setDisplayName(Utils.color("&6Pumkinator's Pumpkin"));
				 pumpkin.setItemMeta(pumpkinMeta); 
				 
				 Player target = Bukkit.getPlayer(args[1]);
				 PlayerInventory inv = target.getInventory();
				 int max = Integer.parseInt(args[2]);
				 for (int i = 0; i < max; i++) {
					 inv.addItem(new ItemStack[] { pumpkin });
				 }
				 target.sendMessage(Utils.color("&6You have been given &b&l" + max + " &6halloween pumpkins."));
			 }
		 }else
			 p.sendMessage(ChatColor.RED + "No permission!");

				return false;
	}
	
}
