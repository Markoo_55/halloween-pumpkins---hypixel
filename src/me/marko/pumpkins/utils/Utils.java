package me.marko.pumpkins.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class Utils {

	public static String color(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	public static void msgConsole(String s) {
		Bukkit.getConsoleSender().sendMessage(color("&6[Halloween] " + s));
	}
}
